# Sketch Go Course

## Organizing Your Application

* [Structuring Applications in Go](https://medium.com/@benbjohnson/structuring-applications-in-go-3b04be4ff091)
* [Go Project Structure Best Practices](https://tutorialedge.net/golang/go-project-structure-best-practices/)
* [github.com/golang-standards](https://github.com/golang-standards/project-layout): An unofficial standard based on historical and Go community idioms.

## Recommended Libraries

The Go standard library is pretty good, but there are a few third-party ones we recommended.

* Unit testing: [testify](https://github.com/stretchr/testify)
* Logging: [logrus](https://github.com/Sirupsen/logrus) (This seems to be in maintenance mode now, alternatives listed on the README)
* HTTP router/dispatcher: [gorilla/mux](https://github.com/gorilla/mux)

## Interesting Reads on Go

* [The Go Blog: Go's Declaration Syntax](https://blog.golang.org/declaration-syntax)
* [The Go Blog: Error Handling in Go](https://blog.golang.org/error-handling-and-go)
* [The Go Blog: Defer, Panic, and Recover](https://blog.golang.org/defer-panic-and-recover)
* [Pointers in Go](https://dave.cheney.net/2014/03/17/pointers-in-go)
* [Shadowed variables](https://yourbasic.org/golang/gotcha-shadowing-variables/)